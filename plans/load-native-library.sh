#!/bin/sh

cat << EOF > "rep.java"
class rep { public static void main(String[] args) { org.fusesource.jansi.internal.JansiLoader.initialize(); } }
EOF

/usr/lib/jvm/java-11-openjdk/bin/javac -cp "/usr/lib/java/jansi/jansi.jar" "rep.java" &&
/usr/lib/jvm/java-11-openjdk/bin/java -cp "/usr/lib/java/jansi/jansi.jar:." "rep"

result=$?
rm -rf "rep.java" "rep.class"
exit ${result}
